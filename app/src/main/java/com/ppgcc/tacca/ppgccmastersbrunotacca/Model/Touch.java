package com.ppgcc.tacca.ppgccmastersbrunotacca.Model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import java.util.Objects;

@Entity(tableName = "touch", primaryKeys={"top_pos", "left_pos"})
public class Touch implements Comparable<Touch> {

    @NonNull
    @ColumnInfo(name = "top_pos")
    private short topPos;

    @NonNull
    @ColumnInfo(name = "left_pos")
    private short leftPos;

    @ColumnInfo(name = "count")
    private long count;

    public Touch(@NonNull short topPos, @NonNull short leftPos, long count) {
        this.topPos = topPos;
        this.leftPos = leftPos;
        this.count = count;
    }

    @NonNull
    public short getTopPos() {
        return topPos;
    }

    public void setTopPos(@NonNull short topPos) {
        this.topPos = topPos;
    }

    @NonNull
    public short getLeftPos() {
        return leftPos;
    }

    public void setLeftPos(@NonNull short leftPos) {
        this.leftPos = leftPos;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String toString() {
        return "["+topPos+"; "+leftPos+"; "+count+"]";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Touch touch = (Touch) o;
        return getTopPos() == touch.getTopPos() &&
                getLeftPos() == touch.getLeftPos();
    }

    @Override
    public int hashCode() {

        return Objects.hash(getTopPos(), getLeftPos());
    }

    @Override
    public int compareTo(Touch o) {
        if (this.topPos>o.topPos) {
            return 1;
        } else if (this.topPos<o.topPos) {
            return -1;
        } else {
            if (this.leftPos>o.leftPos) {
                return 1;
            } else if (this.leftPos<o.leftPos) {
                return -1;
            }
        }

        return 0;
    }
}
