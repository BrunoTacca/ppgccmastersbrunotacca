package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.ppgcc.tacca.ppgccmastersbrunotacca.Model.Touch;

import java.util.ArrayList;
import java.util.List;

public class DrawView extends View {

    List<Touch> listAllTouch;
    Paint paint = new Paint();
    Context context;
    Canvas canvas;
    TouchViewModel mTouchViewModel;
    long max;
    float textSize;
    float strokeWidth;
    Touch actualTouch;
    List<Point> myBubblePath = new ArrayList<>();
    int bubbleWidth;

    // Data from MotionEvent
    float x;
    float y;
    Integer action = null;
    boolean touching = false;


    public DrawView(Context c, TouchViewModel vm) {
        super(c);
        Log.d("DrawView","DrawingView");
        context=c;
        mTouchViewModel = vm;
        RecordTouchUtil.mTouchViewModel = vm;
        max = mTouchViewModel.getMaxCount();
        textSize = 14 * GlobalParams.SCREEN_SCALED_DENSITY;
        strokeWidth = 2f;
        updateBubbleWidth();
    }

    public void updateBubbleWidth() {
        bubbleWidth = (int)textSize*(String.valueOf(max).length());
        if((String.valueOf(max).length())<2)
            bubbleWidth = bubbleWidth*2;
//        System.out.println("bubbleWidth: "+bubbleWidth);
    }

    @Override
    public void onDraw(Canvas canvas) {
        System.out.println("DrawView.onDraw()");

        max = mTouchViewModel.getMaxCount();
        updateBubbleWidth();
//        System.out.println("MAX: "+mTouchViewModel.getMaxCount()+" SUM: "+mTouchViewModel.getSumCount());

        ColorGradientUtil.updateHotmapColorPoints(max);
        int size = GlobalParams.SCREEN_SQUARE_SIZE;
        listAllTouch = mTouchViewModel.getAllTouchs();

        for(Touch t : listAllTouch) {

            // RGB Color on the Spectrum
//            RGB heatmapSquareColor = ColorGradientUtil.getColorByValue(t.getCount());
//            System.out.println(heatmapSquareColor);

            // FILL
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(ColorGradientUtil.getIntColorByValue(t.getCount()));
            canvas.drawRect(t.getTopPos(), t.getLeftPos(), t.getTopPos()+size, t.getLeftPos()+size, paint);

            // STROKE
//            paint.setStyle(Paint.Style.STROKE);
//            paint.setColor(Color.BLACK);
//            paint.setStrokeWidth(2);
//            canvas.drawRect(t.getTopPos(), t.getLeftPos(), t.getTopPos()+size, t.getLeftPos()+size, paint);

            // TEXT
//            paint.setColor(Color.WHITE);
//            paint.setStyle(Paint.Style.FILL);
//            paint.setTextSize(size/2);
//            canvas.drawText(""+t.getCount(), t.getTopPos(), t.getLeftPos()+(size/2), paint);
//            canvas.drawText(""+i, t.getTopPos(), t.getLeftPos()+(size/2), paint);

        }

        if(touching) {
            drawHeatmapSubtitle(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        x = event.getX();
        y = event.getY();
        action = event.getAction();

        if (action != null) {

            switch (action) {
                case (MotionEvent.ACTION_DOWN):
                    touching = true;
                    actualTouch = RecordTouchUtil.putInCachedList(x, y);
                    break;
                case (MotionEvent.ACTION_MOVE):
                    touching = true;
                    actualTouch = RecordTouchUtil.putInCachedList(x, y);
                    break;
                case (MotionEvent.ACTION_UP):
                    touching = false;
                    actualTouch = RecordTouchUtil.putInCachedList(x, y);
                    break;
                default:
                    break;
            }
        }

        actualTouch = mTouchViewModel.getTouch(actualTouch.getTopPos(),actualTouch.getLeftPos());

        this.invalidate();

//        if(RecordTouchUtil.listTouchCached.size()==GlobalParams.TOUCH_UPDATE_INTERVAL) {
//            this.invalidate();
//        }

        return true;
    }


    public void drawHeatmapSubtitle(Canvas canvas) {
        if(actualTouch!=null) {
            drawColorSpectrumSubtitle(canvas);
            drawLocationValueBubble(canvas);
        }
    }

    public void drawColorSpectrumSubtitle(Canvas canvas) {

        // Calculate the size of the rectangle
        // Width will be 90% of the screen, 5% margin each side.
        // Height will be 3% of the screen, 2% margin each side.
        int width = (int) (GlobalParams.SCREEN_WIDTH * 0.90);
        int widthMargin = (int) (GlobalParams.SCREEN_WIDTH * 0.05);
        int height = (int) (GlobalParams.SCREEN_HEIGHT * 0.03);
        int heightMargin = (int) (GlobalParams.SCREEN_HEIGHT * 0.02);

        // Calculate de placement (top or bottom) according to the actual touch (x,y)
        boolean placeAtTop = true;
        System.out.println("height: "+height+" x: "+x+" y: "+y);
        if(y<(height*4))
            placeAtTop = false;

        // Calculate the absolute placement coordinates
        int left = 0;
        int top = 0;
        int right = left+width;
        int bottom = top+height;
        if(placeAtTop) {
            top = heightMargin;
            left = widthMargin;
            right = left+width;
            bottom = top+height;
        } else {
            right = GlobalParams.SCREEN_WIDTH - widthMargin;
            bottom = GlobalParams.SCREEN_HEIGHT - (heightMargin*2);
            top = bottom-height;
            left = right-width;
        }

        // Draw the fill
        int pixelsAmount = width-((int)strokeWidth*2);
        int leftFill;
        int topFill = top+(int)strokeWidth;
        int rightFill;
        int bottomFill = bottom-(int)strokeWidth;
        int colorStepSize = 1;
        long oldMax = max;
        List<Integer> realHeatpmapColorPoints = new ArrayList<>(GlobalParams.DEFAULT_HOTMAP_COLOR_POINTS);
        ColorGradientUtil.updateHotmapColorPoints(pixelsAmount);
        List<Integer> subtitleHeatpmapColorPoints = new ArrayList<>(GlobalParams.DEFAULT_HOTMAP_COLOR_POINTS);
        for(int i=0; i<pixelsAmount; i+=colorStepSize) {
            paint.setColor(ColorGradientUtil.getIntColorByValue(i));
            paint.setStyle(Paint.Style.FILL);

            leftFill = (left+(int)strokeWidth+i);
            rightFill = leftFill+colorStepSize;
            canvas.drawRect(leftFill, topFill, rightFill, bottomFill, paint);
        }

        // Draw the values on the Heatpmap Subtitle
        int xPosText = left;
        int yPosText = bottom+heightMargin;
        for(Integer ci=0; ci<subtitleHeatpmapColorPoints.size(); ci++) {
            // Value to plot
            int value = realHeatpmapColorPoints.get(ci);
            int subtitleColorPosition = subtitleHeatpmapColorPoints.get(ci);
//            System.out.println("value: "+value);
            paint.setColor(GlobalParams.DEFAULT_HOTMAP_SUBTITLE_STROKE_COLOR.toInt());
            paint.setStyle(Paint.Style.FILL);
            paint.setTextSize(textSize);
            xPosText = left+subtitleColorPosition;
            // Draw a point to better accurate the text value
            canvas.drawCircle(xPosText,bottom,strokeWidth*2,paint);
            // Draw the text centered
            drawTextCentered(""+value,xPosText,yPosText,paint,canvas);

        }
        ColorGradientUtil.updateHotmapColorPoints(oldMax);

        // Draw a small rectangle on the touch actual position
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.FILL);
        int w = ColorGradientUtil.getSpectrumPointValueIndex(actualTouch.getCount());
        double per = ColorGradientUtil.getColorImpactPercentage(actualTouch.getCount());
        double wPer = (subtitleHeatpmapColorPoints.get(w+1)-subtitleHeatpmapColorPoints.get(w))*per;
        int subtitleSpectrumActualPosition = widthMargin;
        if(wPer>0)
            subtitleSpectrumActualPosition = (int)(subtitleHeatpmapColorPoints.get(w)+wPer+widthMargin);
//        System.out.println("subtitleHeatpmapColorPoints.get(w: "+subtitleHeatpmapColorPoints.get(w));
//        System.out.println("wPer: "+wPer);
//        System.out.println("widthMargin: "+widthMargin);
//        System.out.println("subtitleSpectrumActualPosition: "+subtitleSpectrumActualPosition);
        drawTriangleCentered(subtitleSpectrumActualPosition,(bottom),20,paint,canvas);

        // Draw the border
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(GlobalParams.DEFAULT_HOTMAP_SUBTITLE_STROKE_COLOR.toInt());
        paint.setStrokeWidth(strokeWidth);
        canvas.drawRect(left, top, right, bottom, paint);

    }

    private void drawTriangleCentered(int x, int y, int radius, Paint paint, Canvas canvas) {
        paint.setStrokeWidth(2);
        paint.setColor(GlobalParams.DEFAULT_HOTMAP_SUBTITLE_STROKE_COLOR.toInt());
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setAntiAlias(true);

        Point point1_draw = new Point();
        Point point2_draw = new Point();
        Point point3_draw = new Point();

        // Top tip
        point1_draw.x = x;
        point1_draw.y = y-radius;

        // Left tip
        point2_draw.x = x-(radius/2);
        point2_draw.y = y+(2*radius/3);

        // Right tip
        point3_draw.x = x+(radius/2);
        point3_draw.y = y+(2*radius/3);

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(point1_draw.x,point1_draw.y);
        path.lineTo(point2_draw.x,point2_draw.y);
        path.lineTo(point3_draw.x,point3_draw.y);
        path.lineTo(point1_draw.x,point1_draw.y);
        path.close();

        canvas.drawPath(path, paint);

    }

    private void drawTextCentered(String text, int x, int y, Paint paint, Canvas canvas) {
        int xPos = x - (int)(paint.measureText(text)/2);
        int yPos = (int) (y - ((paint.descent() + paint.ascent()) / 2)) ;

        canvas.drawText(text, xPos, yPos, paint);
    }


    public void updateTextBubblePoints() {
        // Clear
        myBubblePath.clear();

        // Start point
        int xInt = (int)x;
        int yInt = (int)y;
        myBubblePath.add(new Point(xInt,yInt));

        if(bubbleWidth<100)
            addPointToPath(-1*(bubbleWidth),-1*(bubbleWidth));
        else
            addPointToPath(-1*(4*bubbleWidth/5),-1*(4*bubbleWidth/5));
        addPointToPath(-1*(bubbleWidth/2),0);
        addPointToPath(0,-1*(bubbleWidth/2));
        addPointToPath(bubbleWidth,0);
        addPointToPath(0,(bubbleWidth/2));
        addPointToPath(-1*(bubbleWidth/3),0);
    }

    private void addPointToPath(int moveX, int moveY) {
        myBubblePath.add(new Point(myBubblePath.get(myBubblePath.size()-1).x+moveX, myBubblePath.get(myBubblePath.size()-1).y+moveY));
    }

    public void drawLocationValueBubble(Canvas canvas) {
        updateTextBubblePoints();

        Paint paint = new Paint();

        // Create the path
        Path path = new Path();
        path.moveTo(myBubblePath.get(0).x, myBubblePath.get(0).y);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND); // set the paint cap to round too
        paint.setPathEffect(new CornerPathEffect(18)); // set the path effect when they join.
        for (int i = 1; i < myBubblePath.size(); i++){
            path.lineTo(myBubblePath.get(i).x, myBubblePath.get(i).y);
        }
        path.close();

        // Draw the fill
        if(GlobalParams.DEFAULT_HOTMAP_COLOR_BLACKISCOLD)
            paint.setColor(Color.argb(150,0,0,0));
        else
            paint.setColor(Color.argb(150,255,255,255));

        paint.setStyle(Paint.Style.FILL);
        canvas.drawPath(path, paint);

        // Draw the stroke
        paint.setColor(GlobalParams.DEFAULT_HOTMAP_SUBTITLE_STROKE_COLOR.toInt());
        paint.setAntiAlias(true);
        paint.setStrokeWidth(strokeWidth*2);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawPath(path, paint);

        // Draw the text
        paint.setColor(GlobalParams.DEFAULT_HOTMAP_SUBTITLE_STROKE_COLOR.toInt());
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(textSize);
        drawTextCentered(""+actualTouch.getCount(), myBubblePath.get(1).x, myBubblePath.get(1).y-(bubbleWidth/4),paint,canvas);

    }

}
