package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import com.ppgcc.tacca.ppgccmastersbrunotacca.Model.Touch;

import java.util.List;

public class StartDraw extends AppCompatActivity {
    DrawView drawView;
    TouchViewModel mTouchViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String colormode = preferences.getString("colormode","0");
        Log.d("StartDraw.onCreate","colormode: "+colormode);
        GlobalParams.setColorSpectrum(Integer.parseInt(colormode));


        mTouchViewModel = ViewModelProviders.of(this).get(TouchViewModel.class);

        mTouchViewModel.getAllTouchsLive().observe(this, new Observer<List<Touch>>() {
            @Override
            public void onChanged(@Nullable final List<Touch> touchs) {
                // Update the cached copy of the words in the adapter.
            }
        });

        drawView = new DrawView(this, mTouchViewModel);
        Log.d("StartDraw","onCreate");
        setContentView(drawView);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String colormode = preferences.getString("colormode","0");
        Log.d("StartDraw.attachBaseContext","colormode: "+colormode);
        super.attachBaseContext(MyContextWrapper.wrap(newBase,colormode));
    }


}
