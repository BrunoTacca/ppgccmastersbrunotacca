package com.ppgcc.tacca.ppgccmastersbrunotacca;


import android.graphics.Color;

import java.util.ArrayList;

public class ColorGradientUtil {

   public static void updateHotmapColorPoints(long max) {

        if(max>0) {
            GlobalParams.DEFAULT_HOTMAP_COLOR_POINTS = new ArrayList<>();
            int spectrumSize = GlobalParams.DEFAULT_HOTMAP_COLOR_SPECTRUM.size();
            double step = ((double)max/(double)(spectrumSize-1));
//            System.out.println("spectrumSize: "+spectrumSize+" max: "+max+" step: "+step);
            for (int i=0; i<spectrumSize; i++) {
               GlobalParams.DEFAULT_HOTMAP_COLOR_POINTS.add((int)(i*step));
            }

//            System.out.println(GlobalParams.DEFAULT_HOTMAP_COLOR_POINTS);
        }
    }

    static double k = 100;
    static double a = 5;
    static double b = -0.1;
    static int sigmoidRange = 100;

    public static int getIntColorByValue(long value) {
        RGB color = getColorByValue(value);
        return Color.argb(255,color.r,color.g,color.b);
    }

    public static double getColorImpactPercentage(long value) {
        int userInputColorIndex = getSpectrumPointValueIndex(value);

        double minMaxInputDiff = GlobalParams.DEFAULT_HOTMAP_COLOR_POINTS.get(userInputColorIndex+1)-GlobalParams.DEFAULT_HOTMAP_COLOR_POINTS.get(userInputColorIndex);
        double userDiffToMin = value - GlobalParams.DEFAULT_HOTMAP_COLOR_POINTS.get(userInputColorIndex);
        double x = userDiffToMin/minMaxInputDiff;

        double sigmoid = sigmoid(x*sigmoidRange);
        double impactPercentage = sigmoid/100;

        return impactPercentage;
    }

    public static RGB getColorByValue(long value) {

        if(value>0) {
            int userInputColorIndex = getSpectrumPointValueIndex(value);
//            System.out.println("Value: "+value+" index: "+userInputColorIndex);

            RGB colorMin = GlobalParams.DEFAULT_HOTMAP_COLOR_SPECTRUM.get(userInputColorIndex);
            RGB colorMax = GlobalParams.DEFAULT_HOTMAP_COLOR_SPECTRUM.get(userInputColorIndex+1);

            double impactPercentage = getColorImpactPercentage(value);

//            System.out.println("impactPercentage: "+impactPercentage+" colorMin: "+colorMin);
//            Log.d("COLOR-DEBUG:","x*sigmoidRange: "+(x*sigmoidRange)+" sigmoid/100: "+(sigmoid/100));
//            Log.d("COLOR-DEBUG:","sigmoid: "+sigmoid+" impactPercentage: "+impactPercentage);

            int redColorImpact = getColorImpactValueBetweenRange(colorMin.r,colorMax.r,impactPercentage);
            int greenColorImpact = getColorImpactValueBetweenRange(colorMin.g,colorMax.g,impactPercentage);
            int blueColorImpact = getColorImpactValueBetweenRange(colorMin.b,colorMax.b,impactPercentage);
            /*
            Log.d("COLOR-DEBUG:","redColorImpact: "+redColorImpact+" greenColorImpact: "+greenColorImpact+" blueColorImpact: "+blueColorImpact);
            System.out.println("ColorRange: "+colorMin+"~"+colorMax
                    +" minMaxInputDiff: "+minMaxInputDiff
                    +" userDiffToMin: "+userDiffToMin
                    +" x: "+x
                    +" y: "+sigmoid
                    +" impactPercentage: "+impactPercentage
            );
            */
            RGB gradientRealLocationColor = new RGB(colorMin.r+redColorImpact,colorMin.g+greenColorImpact,colorMin.b+blueColorImpact);

            return gradientRealLocationColor;
        }

        return GlobalParams.DEFAULT_HOTMAP_COLOR_SPECTRUM.get(0);
    }

    public static int getColorImpactValueBetweenRange(int lowColor, int highColor, double percentageValue) {
        int range = highColor - lowColor;
        return (int)(percentageValue*range);
    }


    public static int getSpectrumPointValueIndex(long value) {
        int index;
        if(value==0) return 0;
        for(index=0; index<GlobalParams.DEFAULT_HOTMAP_COLOR_POINTS.size()-1; index++) {
//            System.out.println("value: "+value+" index:"+index+" GlobalParams.DEFAULT_HOTMAP_COLOR_POINTS.get(index+1): "+GlobalParams.DEFAULT_HOTMAP_COLOR_POINTS.get(index+1)+" ");
            if(value<=GlobalParams.DEFAULT_HOTMAP_COLOR_POINTS.get(index+1)) {
                return index;
            }
        }
        return index-1;
    }

    public static double sigmoid(double x) {
//        Log.d("SIGMOID-DEBUG: ","x:"+x+" k:"+k+" E:"+Math.E+" a:"+a+" b:"+b);
        return (k/(1 + Math.pow(Math.E,(a+b*x))));
    }



}
