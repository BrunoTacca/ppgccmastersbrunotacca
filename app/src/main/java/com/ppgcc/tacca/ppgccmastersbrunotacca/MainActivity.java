package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    TextView txtPlayPauseStatus;
    ImageButton btnPlayPause;
    Boolean btnPlayPauseStatus = false;
    TextView txtMenuUsername;

    TouchViewModel mTouchViewModel;

    NotificationCompat.Builder mBuilder;
    NotificationManager notificationManager;

    public static final String NOTIFICATION_CHANNEL_ID = "4655";
    public static final String NOTIFICATION_CHANNEL_NAME = "4655";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Start global params
        DisplayMetrics displaymetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        GlobalParams.setGlobalParams(displaymetrics);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnPlayPause = findViewById(R.id.playToPauseButton);
        txtPlayPauseStatus  = findViewById(R.id.playToPauseStatus);


        btnPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePlayPauseButton();
                sendNotification();
            }
        });

        updatePlayPauseButtonText();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.manu_drawer_open, R.string.manu_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mTouchViewModel = ViewModelProviders.of(this).get(TouchViewModel.class);

//        mTouchViewModel.deleteAll();
//        System.out.println("ADDING OBSERVER!!! >>>> mTouchViewModel.getAllTouchs().onChanged");
//        mTouchViewModel.getAllTouchsLive().observe(this, new Observer<List<Touch>>() {
//            @Override
//            public void onChanged(@Nullable final List<Touch> touchs) {
//                // Update the cached copy of the words in the adapter.
//                long sum = 0;
//                for(Touch t : touchs) sum += t.getCount();
//                Log.d("SUM: ",""+sum);
//                sum = 0;
//                for(Touch t : mTouchViewModel.getAllTouchsLive().getValue()) sum += t.getCount();
//                Log.d("SUM LIVE: ",""+sum);
//
//            }
//        });

        System.out.println(" ---------------------------------- ");
        System.out.println(mTouchViewModel.getAllTouchs());
        System.out.println(" ---------------------------------- ");


        //Notification Channel
        notificationManager = (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);
        }

        mBuilder = new NotificationCompat.Builder(getBaseContext(), NOTIFICATION_CHANNEL_ID);
        mBuilder.setSmallIcon(R.drawable.ic_menu_record);
    }

    private void updatePlayPauseButton() {
        btnPlayPause.setImageResource(
            btnPlayPauseStatus?
                    R.drawable.pause_to_play_animation:
                    R.drawable.play_to_pause_animation
        );
        btnPlayPauseStatus = !btnPlayPauseStatus;
        updatePlayPauseButtonText();

        Drawable icon = btnPlayPause.getDrawable();
        if(icon instanceof AnimatedVectorDrawable) {
            ((AnimatedVectorDrawable) icon).start();
        } else if (icon instanceof AnimatedVectorDrawableCompat) {
            ((AnimatedVectorDrawableCompat) icon).start();
        }
    }

    private void updatePlayPauseButtonText() {
        txtPlayPauseStatus.setText(
                btnPlayPauseStatus?
                        R.string.play_icon_playing:
                        R.string.play_icon_stopped
        );
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        Toast.makeText(getApplicationContext(), "onCreateOptionsMenu()", Toast.LENGTH_SHORT).show();
        getMenuInflater().inflate(R.menu.extra_menu, menu);
        updateUsername();
        return true;
    }


    public void updateUsername() {
        txtMenuUsername = findViewById(R.id.txtUsername);
        txtMenuUsername.setText(GlobalParams.USER_USERNAME);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        Toast.makeText(getApplicationContext(), "onOptionsItemSelected()", Toast.LENGTH_SHORT).show();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_preferences) {
//            Toast.makeText(getApplicationContext(), "action_preferences", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, MyPreferenceActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    DrawView drawView;

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

//        Toast.makeText(getApplicationContext(), "onNavigationItemSelected", Toast.LENGTH_SHORT).show();
        if (id == R.id.nav_home) {
            // Handle the camera action
//            Toast.makeText(getApplicationContext(), "nav_home", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_heat_map) {
//            Toast.makeText(getApplicationContext(), "nav_heat_map", Toast.LENGTH_SHORT).show();
//
//            drawView = new DrawView(this);
//            setContentView(drawView);
//            drawView = findViewById(R.id.drawViewComponent);
//            return true;

            Intent i = new Intent(this, StartDraw.class);
//            Intent i = new Intent(this, StartDrawing.class);
            startActivity(i);

        } else if (id == R.id.nav_filter) {
//            Toast.makeText(getApplicationContext(), "Filter", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_record) {
//            Toast.makeText(getApplicationContext(), "Record", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, RecordScreenActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_share) {
//            Toast.makeText(getApplicationContext(), "nav_share", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_support) {
//            Toast.makeText(getApplicationContext(), "nav_support", Toast.LENGTH_SHORT).show();

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
//        System.out.println("MainActivity.onRestart() ");
        updateUsername();
//        MyPreferenceGeneralFragment.set_app_interface_language();
    }

    public void sendNotification() {
        if(btnPlayPauseStatus) {
            mBuilder.setContentTitle(getResources().getString(R.string.play_icon_playing));
            mBuilder.setContentText(mTouchViewModel.getMaxCount()+" "+getResources().getString(R.string.play_icon_touch_count_desc_playing));
        } else {
            mBuilder.setContentTitle(getResources().getString(R.string.play_icon_stopped));
            mBuilder.setContentText(mTouchViewModel.getMaxCount()+" "+getResources().getString(R.string.play_icon_touch_count_desc_stopped));
        }
        notificationManager.cancelAll();
        notificationManager.notify((int)(System.currentTimeMillis()/1000), mBuilder.build());
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String locale = preferences.getString("language","en");
        String username = preferences.getString("username",""+R.string.settings_username);
        GlobalParams.USER_USERNAME = username;
        super.attachBaseContext(MyContextWrapper.wrap(newBase,locale));
    }


}
