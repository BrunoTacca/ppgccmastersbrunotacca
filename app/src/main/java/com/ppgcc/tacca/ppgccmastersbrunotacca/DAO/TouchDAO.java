package com.ppgcc.tacca.ppgccmastersbrunotacca.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.ppgcc.tacca.ppgccmastersbrunotacca.Model.Touch;

import java.util.List;

@Dao
public interface TouchDAO {

    @Insert
    void insert(Touch touch);

    @Update
    void update(Touch touch);

    @Query("UPDATE touch SET count = count+100 WHERE top_pos IN (:topPos) AND left_pos IN (:leftPos)")
    int updateIncreaseTouchCounts(List<Short> topPos, List<Short> leftPos);

    @Query("UPDATE touch SET count = count + :increment WHERE top_pos = :topPos AND left_pos = :leftPos")
    int updateIncreaseTouchCounter(short topPos, short leftPos, short increment);

    @Query("DELETE FROM touch")
    void deleteAll();

    @Query("SELECT * from touch ORDER BY top_pos ASC")
    LiveData<List<Touch>> getAllTouchsLive();

    @Query("SELECT * from touch ORDER BY top_pos ASC")
    List<Touch> getAllTouchs();

}
