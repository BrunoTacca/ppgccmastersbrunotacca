package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.ppgcc.tacca.ppgccmastersbrunotacca.DAO.TouchDAO;
import com.ppgcc.tacca.ppgccmastersbrunotacca.Model.Touch;

@Database(entities = {Touch.class}, version = 3)
public abstract class TouchDatabase extends RoomDatabase {

    private static volatile TouchDatabase INSTANCE;

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    static TouchDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (TouchDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            TouchDatabase.class, "touch_database")
                            .fallbackToDestructiveMigration()
                            .allowMainThreadQueries()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract TouchDAO touchDAO();

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final TouchDAO mDao;

        PopulateDbAsync(TouchDatabase db) {
            mDao = db.touchDAO();
        }

        @Override
        protected Void doInBackground(final Void... params) {

            System.out.println("TouchDatabase.doInBackground()");

            System.out.println("ScreenSize: "+GlobalParams.SCREEN_HEIGHT+" x "+GlobalParams.SCREEN_WIDTH+" / "+GlobalParams.SCREEN_SQUARE_SIZE);
            int recommendedEntries = (GlobalParams.SCREEN_HEIGHT*GlobalParams.SCREEN_WIDTH)/(GlobalParams.SCREEN_SQUARE_SIZE*GlobalParams.SCREEN_SQUARE_SIZE);
            System.out.println("recommendedEntries: " + recommendedEntries);
            int touchEntries = 0;
//            System.out.println("mDao.getAllTouchs(): "+(mDao.getAllTouchs()==null?"NULL":"NOT NULL"));
//            System.out.println("mDao.getAllTouchsLive().getValue(): "+(mDao.getAllTouchsLive().getValue()==null?"NULL":"NOT NULL"));

            if(mDao.getAllTouchs()!=null) {
                touchEntries = mDao.getAllTouchs().size();
                System.out.println("touchEntries: " + touchEntries);
                System.out.println("GlobalParams.SCREEN_RECOMMENDED_SQUARES: " + GlobalParams.SCREEN_RECOMMENDED_SQUARES);
            }

            if(touchEntries!=GlobalParams.SCREEN_RECOMMENDED_SQUARES) {
                mDao.deleteAll();

                Touch touch;
                for (int y = 0; y < GlobalParams.SCREEN_HEIGHT; y += GlobalParams.SCREEN_SQUARE_SIZE) {
                    for (int x = 0; x < GlobalParams.SCREEN_WIDTH; x += GlobalParams.SCREEN_SQUARE_SIZE) {

                        System.out.println("Inserting... [" + x + ";" + y + "]");
                        touch = new Touch((short) x, (short) y, 0);
                        mDao.insert(touch);
                    }
                }
            }

            return null;
        }
    }

}
