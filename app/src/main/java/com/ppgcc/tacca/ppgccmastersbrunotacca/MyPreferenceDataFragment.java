package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.Preference;
import android.util.Log;

import java.util.Arrays;

public class MyPreferenceDataFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences_data);
        System.out.println("MyPreferenceDataFragment.onCreate()");

        updateColormode(getPreferenceScreen().getSharedPreferences());
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        System.out.println("MyPreferenceDataFragment.onCreatePreferences()");

    }

    @Override
    public void onResume() {
        super.onResume();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        System.out.println("MyPreferenceDataFragment.onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        System.out.println("MyPreferenceDataFragment.onPause()");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        System.out.println("MyPreferenceDataFragment.onConfigurationChanged()");

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.i("Pref changed", "Settings key changed: " + key);

        if (key.equals("colormode")) {
            updateColormode(sharedPreferences);
//            Toast.makeText(getActivity(), R.string.settings_language_apply, Toast.LENGTH_LONG).show();
        }

    }

    public void updateColormode(SharedPreferences sharedPreferences) {
        String key = "colormode";

        // Language selected
        String selectedColormodeValue = sharedPreferences.getString(key, "0");
        System.out.println(Arrays.asList(getResources().getStringArray(R.array.listColorModesValues)));
        System.out.println("selectedColormodeValue: "+selectedColormodeValue);

        // Recover de description of the value
        int keyIndex = Arrays.asList(getResources().getStringArray(R.array.listColorModesValues)).indexOf(selectedColormodeValue);
        if(keyIndex<0 && Integer.parseInt(selectedColormodeValue)>=0)
            keyIndex = Integer.parseInt(selectedColormodeValue);
        String selectedColormodeDesc = getResources().getStringArray(R.array.listColorModes)[keyIndex];

        // Set summary to be the user-description for the selected value
        Preference exercisesPref = findPreference(key);
        exercisesPref.setSummary(getResources().getString(R.string.settings_heatmapcolormode_desc)+" "+selectedColormodeDesc);
    }


}
