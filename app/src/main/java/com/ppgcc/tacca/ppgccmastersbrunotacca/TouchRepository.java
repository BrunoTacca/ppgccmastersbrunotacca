package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.ppgcc.tacca.ppgccmastersbrunotacca.DAO.TouchDAO;
import com.ppgcc.tacca.ppgccmastersbrunotacca.Model.Touch;

import java.util.ArrayList;
import java.util.List;

public class TouchRepository {

    private TouchDAO mTouchDAO;
    private LiveData<List<Touch>> mAllTouchsLive;
    private List<Touch> mAllTouchs;

    TouchRepository(Application application) {
        TouchDatabase db = TouchDatabase.getDatabase(application);
        mTouchDAO = db.touchDAO();
        mAllTouchsLive = mTouchDAO.getAllTouchsLive();
        mAllTouchs = mTouchDAO.getAllTouchs();
    }

    LiveData<List<Touch>> getAllTouchsLive() {
        return mAllTouchsLive;
    }

    List<Touch> getAllTouchs() {
        return mAllTouchs;
    }

    public void insert(Touch touch) {
        new insertAsyncTask(mTouchDAO).execute(touch);
    }

    public void updateTouchListCounters(Touch[] listTouchs) {
        new updateTouchListCounterAsyncTask(mTouchDAO).execute(listTouchs);
    }

    public void updateTouchCounter(Touch[] listTouchs) {
        new updateTouchCounterAsyncTask(mTouchDAO).execute(listTouchs);
    }

    public void deleteAll() {
        new deleteAllAsyncTask(mTouchDAO).execute();
    }

    private static class insertAsyncTask extends AsyncTask<Touch, Void, Void> {

        private TouchDAO mAsyncTaskDao;

        insertAsyncTask(TouchDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Touch... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class updateTouchCounterAsyncTask extends AsyncTask<Touch, Void, Void> {

        private TouchDAO mAsyncTaskDao;

        updateTouchCounterAsyncTask(TouchDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Touch... params) {
            if(params.length>0) {
//                System.out.println("Executing updateTouchCounterAsyncTask: "+params[0].getTopPos()+"; "+params[0].getLeftPos()+"; "+params.length);
                mAsyncTaskDao.updateIncreaseTouchCounter(params[0].getTopPos(), params[0].getLeftPos(), (short)params.length);
            }
            return null;
        }
    }

    private static class updateTouchListCounterAsyncTask extends AsyncTask<Touch, Void, Void> {

        private TouchDAO mAsyncTaskDao;

        updateTouchListCounterAsyncTask(TouchDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Touch... params) {
            List<Short> topPos = new ArrayList<>();
            List<Short> leftPos = new ArrayList<>();

            for (Touch t : params) {
                topPos.add(t.getTopPos());
                leftPos.add(t.getLeftPos());
            }

            mAsyncTaskDao.updateIncreaseTouchCounts(topPos, leftPos);
            return null;
        }
    }

    private static class deleteAllAsyncTask extends AsyncTask<Void, Void, Void> {

        private TouchDAO mAsyncTaskDao;

        deleteAllAsyncTask(TouchDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }
}
