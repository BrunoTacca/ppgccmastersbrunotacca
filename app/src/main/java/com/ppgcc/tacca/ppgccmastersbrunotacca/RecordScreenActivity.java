package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.view.MotionEvent;
import android.widget.TextView;

public class RecordScreenActivity extends AppCompatActivity {

    static String DEBUG_TAG = "RecordScreen";

    TextView txPressed;
    TextView txMovement;
    TextView txReleased;
    TextView txTouchPressure;
    TextView txTouchSize;

    // Data from MotionEvent
    float x;
    float y;
    Integer action = null;
    float touchPressure;
    float touchSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_screen);

        txPressed = findViewById(R.id.reccordscreen_act_data_pressed);
        txMovement = findViewById(R.id.reccordscreen_act_data_movement);
        txReleased = findViewById(R.id.reccordscreen_act_data_released);
        txTouchPressure = findViewById(R.id.reccordscreen_act_data_touch_pressure);
        txTouchSize = findViewById(R.id.reccordscreen_act_data_touch_size);

        updateTextViewData();

    }

    private void updateTextViewData() {

        if (action != null) {

            switch (action) {
                case (MotionEvent.ACTION_DOWN):
                    updatedTextViewPressed();
                    RecordTouchUtil.putInCachedList(x,y);
                    break;
                case (MotionEvent.ACTION_MOVE):
                    updatedTextViewMovement();
                    RecordTouchUtil.putInCachedList(x,y);
                    break;
                case (MotionEvent.ACTION_UP):
                    updatedTextViewReleased();
                    RecordTouchUtil.putInCachedList(x,y);
                    break;
                default:
                    break;
            }
            updatedTextViewTouchPressure("" + touchPressure);
            updatedTextViewTouchSize("" + touchSize);

        } else {
            updatedTextViewPressed();
            updatedTextViewMovement();
            updatedTextViewReleased();
        }
    }

    private void updatedTextViewPressed() {
        txPressed.setText(getResources().getString(R.string.reccordscreen_act_data_pressed) + " " +
                getResources().getString(R.string.reccordscreen_act_data_x) + x + " " +
                getResources().getString(R.string.reccordscreen_act_data_y) + y);
    }

    private void updatedTextViewMovement() {
        txMovement.setText(getResources().getString(R.string.reccordscreen_act_data_movement) + " " +
                getResources().getString(R.string.reccordscreen_act_data_x) + x + " " +
                getResources().getString(R.string.reccordscreen_act_data_y) + y);
    }

    private void updatedTextViewReleased() {
        txReleased.setText(getResources().getString(R.string.reccordscreen_act_data_released) + " " +
                getResources().getString(R.string.reccordscreen_act_data_x) + x + " " +
                getResources().getString(R.string.reccordscreen_act_data_y) + y);
    }

    private void updatedTextViewTouchPressure(String pressure) {
        txTouchPressure.setText(getResources().getString(R.string.reccordscreen_act_data_touch_pressure) + " " + pressure);
    }

    private void updatedTextViewTouchSize(String size) {
        txTouchSize.setText(getResources().getString(R.string.reccordscreen_act_data_touch_size) + " " + size);
    }

    // This example shows an Activity, but you would use the same approach if you were subclassing a View.
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        x = event.getX();
        y = event.getY();
        action = event.getAction();
        touchPressure = event.getPressure();
        touchSize = event.getSize();
//        Log.d(DEBUG_TAG, "Pressure: " + touchPressure);
//        Log.d(DEBUG_TAG, "Size: " + touchSize);

        updateTextViewData();
        return super.onTouchEvent(event);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String locale = preferences.getString("language", "en");
        super.attachBaseContext(MyContextWrapper.wrap(newBase, locale));
    }


}
