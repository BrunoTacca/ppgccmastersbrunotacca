package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.ppgcc.tacca.ppgccmastersbrunotacca.Model.Touch;

import java.util.List;

public class TouchViewModel extends AndroidViewModel {

    private TouchRepository mRepository;

    private LiveData<List<Touch>> mAllTouchsLive;
    private List<Touch> mAllTouchs;
    private long maxCount;
    private long sumCount;

    public TouchViewModel(Application application) {
        super(application);
        mRepository = new TouchRepository(application);
        mAllTouchsLive = mRepository.getAllTouchsLive();
        mAllTouchs = mRepository.getAllTouchs();
        maxCount = getMaxCount();
        sumCount = getSumCount();
    }

    LiveData<List<Touch>> getAllTouchsLive() {
        return mAllTouchsLive;
    }

    List<Touch> getAllTouchs() {
        if(mAllTouchsLive.getValue()!=null)
            mAllTouchs = mAllTouchsLive.getValue();
        return mAllTouchs;
    }

    public long getMaxCount() {
        long maxCount=Long.MIN_VALUE;
        if(mAllTouchsLive.getValue()!=null) {
            for (Touch t : mAllTouchsLive.getValue())
                if (maxCount < t.getCount()) maxCount = t.getCount();
        } else if(mAllTouchs!=null) {
            for(Touch t : mAllTouchs)
                if(maxCount<t.getCount()) maxCount = t.getCount();
        }
        return maxCount;
    }

    public long getSumCount() {
        long sumCount=0;
        if(mAllTouchsLive.getValue()!=null) {
            for(Touch t : mAllTouchsLive.getValue())
                sumCount+=t.getCount();
        } else if(mAllTouchs!=null) {
            for(Touch t : mAllTouchs)
                sumCount+=t.getCount();
        }
        return sumCount;
    }

    public Touch getTouch(short top, short left) {
        for(Touch t : mAllTouchs) {
            if(t.getTopPos() == top && t.getLeftPos() == left) return t;
        }
        return null;
    }

    public void insert(Touch touch) {
        mRepository.insert(touch);
    }

    public void updateTouchListCounters(List<Touch> listTouchs) {
        Touch[] arrayTouch = new Touch[listTouchs.size()];
        arrayTouch = listTouchs.toArray(arrayTouch);
        mRepository.updateTouchListCounters(arrayTouch);
    }

    public void updateTouchCounter(List<Touch> listTouchs) {
        Touch[] arrayTouch = new Touch[listTouchs.size()];

        arrayTouch = listTouchs.toArray(arrayTouch);
//        System.out.println("listTouchs.size: "+listTouchs.size());
//        System.out.println("arrayTouch.length: "+arrayTouch.length);
//        Log.d("TouchViewModel","Updating entry");
        mRepository.updateTouchCounter(arrayTouch);
    }

    public void deleteAll() {
        mRepository.deleteAll();
    }


}
