package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.graphics.Color;

public class RGB {
	
	int r;
	int g;
	int b;
	
	public RGB(int r, int g, int b) {
		super();
		this.r = r;
		this.g = g;
		this.b = b;
	}

	@Override
	public String toString() {
		return "RGB("+r+","+g+","+b+")";
	}

	public int toInt() {
		return Color.argb(255,r,g,b);
	}

}
