package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.Preference;
import android.util.Log;
import android.widget.Toast;

import java.util.Arrays;

public class MyPreferenceGeneralFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences_general);
        System.out.println("MyPreferenceGeneralFragment.onCreate()");

        updateLanguageSummary(getPreferenceScreen().getSharedPreferences(),"language");
        updateUsername(getPreferenceScreen().getSharedPreferences(),"username");
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        System.out.println("MyPreferenceGeneralFragment.onCreatePreferences()");

    }

    @Override
    public void onResume() {
        super.onResume();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        System.out.println("MyPreferenceGeneralFragment.onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        System.out.println("MyPreferenceGeneralFragment.onPause()");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        System.out.println("MyPreferenceGeneralFragment.onConfigurationChanged()");

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.i("Pref changed", "Settings key changed: " + key);

        if (key.equals("language")) {
            updateLanguageSummary(sharedPreferences, key);
            Toast.makeText(getActivity(), R.string.settings_language_apply, Toast.LENGTH_LONG).show();
        }
        else if(key.equals("username")) {
            updateUsername(sharedPreferences, key);
        }

    }

    public void updateUsername(SharedPreferences sharedPreferences, String key) {
        String selectedLangValue = sharedPreferences.getString(key, ""+R.string.settings_username);
        GlobalParams.USER_USERNAME = selectedLangValue;
    }

    public void updateLanguageSummary(SharedPreferences sharedPreferences, String key) {

        // Language selected
        String selectedLangValue = sharedPreferences.getString(key, "en");
        System.out.println(Arrays.asList(getResources().getStringArray(R.array.listLanguagesValues)));
        System.out.println("selectedLangValue: "+selectedLangValue);
        // Recover de description of the value
        int keyIndex = Arrays.asList(getResources().getStringArray(R.array.listLanguagesValues)).indexOf(selectedLangValue);
        if(keyIndex<0 && Integer.parseInt(selectedLangValue)>=0)
            keyIndex = Integer.parseInt(selectedLangValue);
        String selectedLangDesc = getResources().getStringArray(R.array.listLanguages)[keyIndex];

        // Set summary to be the user-description for the selected value
        Preference exercisesPref = findPreference(key);
        exercisesPref.setSummary(getResources().getString(R.string.settings_language_desc)+" "+selectedLangDesc);
    }


}
