package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.util.DisplayMetrics;

import java.util.ArrayList;
import java.util.List;

public class GlobalParams {

    public static String USER_USERNAME;

    public static int SCREEN_WIDTH;
    public static int SCREEN_HEIGHT;
    public static float SCREEN_SCALED_DENSITY;
    public static int SCREEN_SQUARE_SIZE;
    public static int SCREEN_RECOMMENDED_SQUARES;
    public static int TOUCH_UPDATE_INTERVAL;
    public static int TOUCH_UPDATE_INSERTMOREDEBUG_AMOUNT;

    public static boolean DEFAULT_HOTMAP_COLOR_BLACKISCOLD;
    public static RGB DEFAULT_HOTMAP_SUBTITLE_STROKE_COLOR;
    public static List<RGB> DEFAULT_HOTMAP_COLOR_SPECTRUM;
    public static List<Integer> DEFAULT_HOTMAP_COLOR_POINTS;

    public static void setGlobalParams(DisplayMetrics displaymetrics) {

        TOUCH_UPDATE_INTERVAL = 10;
        TOUCH_UPDATE_INSERTMOREDEBUG_AMOUNT = 0;
        SCREEN_RECOMMENDED_SQUARES = 0;

        SCREEN_WIDTH = displaymetrics.widthPixels;
        SCREEN_HEIGHT = displaymetrics.heightPixels;
        SCREEN_SCALED_DENSITY = displaymetrics.scaledDensity;

        // Square size based on the resolution, higher resolution means larger square.

        // Anything higher than Full HD
        if(SCREEN_HEIGHT>2000 && SCREEN_WIDTH>1150) {
            SCREEN_SQUARE_SIZE = 50;
        } else
        // Full HD (1920x1080)
        if(SCREEN_HEIGHT>1750 && SCREEN_WIDTH>950) {
            SCREEN_SQUARE_SIZE = 40;
        } else
        // HD (1280x720)
        if(SCREEN_HEIGHT>1150 && SCREEN_WIDTH>600) {
            SCREEN_SQUARE_SIZE = 30;
        }
        // Lower than HD
        else {
            SCREEN_SQUARE_SIZE = 20;
        }
        for (int y = 0; y < GlobalParams.SCREEN_HEIGHT; y += GlobalParams.SCREEN_SQUARE_SIZE) {
            for (int x = 0; x < GlobalParams.SCREEN_WIDTH; x += GlobalParams.SCREEN_SQUARE_SIZE) {
                SCREEN_RECOMMENDED_SQUARES++;
            }
        }

        setColorSpectrum(0);

    }

    public static void setColorSpectrum(int option) {
        DEFAULT_HOTMAP_COLOR_SPECTRUM = new ArrayList<>();

        // Option 0
        if(option==0) {
            //YlOrRd
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(255, 255, 204));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(255, 237, 160));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(254, 217, 118));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(254, 178, 76));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(253, 141, 60));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(252, 78, 42));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(227, 26, 28));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(189, 0, 38));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(128, 0, 38));
            DEFAULT_HOTMAP_COLOR_BLACKISCOLD = false;
            DEFAULT_HOTMAP_SUBTITLE_STROKE_COLOR = new RGB(0, 0, 0);
        } else
        if(option==1) {
        // Viridis - Inferno - 10 divisions
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(0, 0, 0));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(27, 12, 65));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(76, 12, 107));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(120, 28, 109));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(165, 44, 96));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(207, 68, 70));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(237, 105, 37));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(251, 153, 6));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(247, 209, 61));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(252, 255, 164));
            DEFAULT_HOTMAP_COLOR_BLACKISCOLD = true;
            DEFAULT_HOTMAP_SUBTITLE_STROKE_COLOR = new RGB(0,255,0);
        } else
        if(option==2) {
            // ColorBlind Safe - CET-CBD1: diverging-protanopic-deuteranopic_bwy_60-95_c32_n256
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(58, 144, 255));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(120, 165, 252));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(161, 186, 249));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(195, 207, 246));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(226, 229, 243));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(234, 228, 214));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(234, 228, 214));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(221, 206, 167));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(206, 185, 121));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(188, 164, 75));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(168, 144, 8));
            DEFAULT_HOTMAP_COLOR_BLACKISCOLD = false;
            DEFAULT_HOTMAP_SUBTITLE_STROKE_COLOR = new RGB(0, 0, 0);
        } else
        if(option==3) {
            // ColorBlind Safe - CET-CBL2: linear-protanopic-deuteranopic_kbw_5-98_c40_n256
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(17, 17, 17));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(16, 54, 103));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(10, 100, 186));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(111, 148, 223));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(228, 197, 60));
            DEFAULT_HOTMAP_COLOR_SPECTRUM.add(new RGB(252, 248, 239));
            DEFAULT_HOTMAP_COLOR_BLACKISCOLD = true;
            DEFAULT_HOTMAP_SUBTITLE_STROKE_COLOR = new RGB(0,255,0);
        }

        // Reinitialize the array of color points
        // This is updated according to the maximum touch counter value
        DEFAULT_HOTMAP_COLOR_POINTS = new ArrayList<>();
        for(RGB rgb : DEFAULT_HOTMAP_COLOR_SPECTRUM) {
            DEFAULT_HOTMAP_COLOR_POINTS.add(-1);
        }

    }


}
