package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.util.Log;

import com.ppgcc.tacca.ppgccmastersbrunotacca.Model.Touch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecordTouchUtil {

    // Cached touch objects
    static List<Touch> listTouchCached = new ArrayList<>();
    static Touch lastCachedTouch = null;
    static int listTouchCachedSize = -1;
    static TouchViewModel mTouchViewModel;

    public static Touch putInCachedList(double x, double y) {
        int top = (int) x / GlobalParams.SCREEN_SQUARE_SIZE;
        int left = (int) y / GlobalParams.SCREEN_SQUARE_SIZE;
        top = top*GlobalParams.SCREEN_SQUARE_SIZE;
        left = left*GlobalParams.SCREEN_SQUARE_SIZE;

//        System.out.println("T: " + top + " L: " + left + " ListSize: " + listTouchCachedSize);
        Touch actualTouch = new Touch((short) top, (short) left, 0);
        if (lastCachedTouch == null || !lastCachedTouch.equals(actualTouch)) {
            if(GlobalParams.TOUCH_UPDATE_INSERTMOREDEBUG_AMOUNT>0)
                for(int i=0; i<GlobalParams.TOUCH_UPDATE_INSERTMOREDEBUG_AMOUNT; i++)
                    listTouchCached.add(actualTouch);

            listTouchCached.add(actualTouch);
            lastCachedTouch = actualTouch;
            listTouchCachedSize = listTouchCached.size();
            Log.d("listTouchCachedSize: ",""+listTouchCachedSize);
        }

        if (listTouchCachedSize > GlobalParams.TOUCH_UPDATE_INTERVAL+GlobalParams.TOUCH_UPDATE_INSERTMOREDEBUG_AMOUNT) {
            // Save List of Data
            Collections.sort(listTouchCached);
            List<Touch> listToInsert = new ArrayList<>();
            Touch previousT = null;
//            System.out.println("listTouchCached: "+listTouchCached);
            for(Touch t : listTouchCached) {
                if(previousT==null) previousT = t;
                if(previousT!=t) {
                    mTouchViewModel.updateTouchCounter(listToInsert);
                    listToInsert.clear();
                }
                listToInsert.add(t);
//                System.out.println("listToInsert.size(): "+listToInsert.size());
            }

            // Clear list
            listTouchCached.clear();
        }
        return actualTouch;

    }
}
