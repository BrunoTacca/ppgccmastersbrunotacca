package com.ppgcc.tacca.ppgccmastersbrunotacca;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceActivity;
import android.support.v7.preference.PreferenceManager;

import java.util.List;

public class MyPreferenceActivity extends PreferenceActivity
{
    @Override
    public void onBuildHeaders(List<Header> target)
    {
        loadHeadersFromResource(R.xml.headers_preference, target);
        System.out.println("MyPreferenceActivity.onBuildHeaders()");
    }

    @Override
    protected boolean isValidFragment(String fragmentName)
    {
        System.out.println("MyPreferenceActivity.isValidFragment(): "+fragmentName);
        if(fragmentName.contains("MyPreferenceGeneralFragment"))
            return MyPreferenceGeneralFragment.class.getName().equals(fragmentName);
        else if(fragmentName.contains("MyPreferenceDataFragment"))
            return MyPreferenceDataFragment.class.getName().equals(fragmentName);

        return false;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String locale = preferences.getString("language","en");
        super.attachBaseContext(MyContextWrapper.wrap(newBase,locale));
    }

}